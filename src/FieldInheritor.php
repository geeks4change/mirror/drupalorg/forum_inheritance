<?php

namespace Drupal\forum_inheritance;

use Drupal\Component\Utility\DiffArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;

class FieldInheritor {

  /**
   * Inherit field changes.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface[] $sourceEntities
   *   The entity.
   * @param \Drupal\Core\Entity\FieldableEntityInterface[] $targetEntities
   *   The target entities.
   * @param string[] $fieldMap
   *   The field map. Not existing fields are silently ignored.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public static function inheritFieldChanges($sourceEntities, $targetEntities, $fieldMap) {
    $targetNeedsSave = self::copyFieldChanges($sourceEntities, $targetEntities, $fieldMap);
    foreach ($targetNeedsSave as $target) {
      $target->save();
    }
  }

  /**
   * Copy field changes but do not save them.
   *
   * @param $sourceEntities
   * @param $targetEntities
   * @param $fieldMap
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
    public static function copyFieldChanges($sourceEntities, $targetEntities, $fieldMap) {
      $targetNeedsSave = [];
      foreach ($sourceEntities as $sourceEntity) {
      /** @var \Drupal\taxonomy\TermInterface $originalSourceEntity */
      $originalSourceEntity = $sourceEntity->original ?? static::emptyEntity($sourceEntity);
      foreach ($targetEntities as $targetIndex => $targetEntity) {
        foreach ($fieldMap as $sourceFieldName => $targetFieldName) {
          if ($sourceEntity->hasField($sourceFieldName) && $targetEntity->hasField($targetFieldName)) {
            $changedTarget = static::inheritOneFieldChange(
              $originalSourceEntity->get($sourceFieldName),
              $sourceEntity->get($sourceFieldName),
              $targetEntity->get($targetFieldName)
            );
            if ($changedTarget) {
              $targetNeedsSave[$targetIndex] = $targetEntity;
            }
          }
        }
      }
    }
    return $targetNeedsSave;
  }

  /**
   * Inherit one field change.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $originalSourceField
   * @param \Drupal\Core\Field\FieldItemListInterface $sourceField
   * @param \Drupal\Core\Field\FieldItemListInterface $targetField
   *
   * @return bool
   *   If the target field has been changed.
   *
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public static function inheritOneFieldChange(FieldItemListInterface $originalSourceField, FieldItemListInterface $sourceField, FieldItemListInterface $targetField) {
    if ($sourceField->getFieldDefinition()->getFieldStorageDefinition()->isMultiple()) {
      return static::inheritOneSetFieldChange($originalSourceField, $sourceField, $targetField);
    }
    else {
      return static::inheritOneSingleFieldChange($originalSourceField, $sourceField, $targetField);
    }
  }

  /**
   * Inherit one field change.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $originalSourceField
   * @param \Drupal\Core\Field\FieldItemListInterface $sourceField
   * @param \Drupal\Core\Field\FieldItemListInterface $targetField
   *
   * @return bool
   *   If the target field has been changed.
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public static function inheritOneSingleFieldChange(FieldItemListInterface $originalSourceField, FieldItemListInterface $sourceField, FieldItemListInterface $targetField) {
    if (!$sourceField->equals($originalSourceField)) {
      $originalTargetField = clone $targetField;
      $targetField->setValue($sourceField->getValue());
      $changed = !$targetField->equals($originalTargetField);
    }
    else {
      $changed = FALSE;
    }
    return $changed;
  }

  /**
   * Inherit one set field change.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $originalSourceField
   * @param \Drupal\Core\Field\FieldItemListInterface $sourceField
   * @param \Drupal\Core\Field\FieldItemListInterface $targetField
   *
   * @return bool
   *   If the target field has been changed.
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public static function inheritOneSetFieldChange(FieldItemListInterface $originalSourceField, FieldItemListInterface $sourceField, FieldItemListInterface $targetField) {
    $currentValues = FieldItemSetHelper::fieldItemListToArray($sourceField);
    $originalValues = FieldItemSetHelper::fieldItemListToArray($originalSourceField);
    $valuesAdded = FieldItemSetHelper::arrayDiff($currentValues, $originalValues);
    $valuesRemoved = FieldItemSetHelper::arrayDiff($originalValues, $currentValues);

    $targetValues = FieldItemSetHelper::fieldItemListToArray($targetField);
    $originalTargetValues = $targetValues;
    $targetValues = FieldItemSetHelper::arrayDiff($targetValues, $valuesRemoved);
    $targetValues = FieldItemSetHelper::arrayUnion($targetValues, $valuesAdded);

    $targetField->setValue($targetValues);

    $changed = FieldItemSetHelper::arrayDiff($targetValues, $originalTargetValues);
    return (bool) $changed;
  }

  /**
   * Create an empty entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $sourceEntity
   *   The source entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The empty entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private static function emptyEntity(EntityInterface $sourceEntity) {
    $entityType = $sourceEntity->getEntityType();
    $entityTypeManager = \Drupal::entityTypeManager();
    $storage = $entityTypeManager->getStorage($sourceEntity->getEntityTypeId());
    $emptyEntity = $storage->create([$entityType->getKey('bundle') => $sourceEntity->bundle()]);
    return $emptyEntity;
  }

}
