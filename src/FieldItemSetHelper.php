<?php

namespace Drupal\forum_inheritance;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;

class FieldItemSetHelper {

  public static function arrayDiff($array1, $array2) {
    $result = [];
    foreach ($array1 as $item1) {
      if (!in_array($item1, $array2, TRUE)) {
        $result[] = $item1;
      }
    }
    return $result;
  }

  public static function arrayUnion($array1, $array2) {
    $result = array_merge($array1, self::arrayDiff($array2, $array1));
    return $result;
  }

  public static function arraySort($array) {
    $result = usort($array, function ($v, $w) {
      return serialize($v) <=> serialize($w);
    });
    return $result;
  }

  public static function fieldItemToArray(FieldItemInterface $fieldItem) {
    $propertyValues = $fieldItem->getValue();
    $callback = self::fieldItemNormalizerCallback($fieldItem->getFieldDefinition());
    $normalized = $callback($propertyValues);
    return $normalized;
  }

  public static function fieldItemListToArray(FieldItemListInterface $fieldItemList) {
    $itemValues = $fieldItemList->getValue();
    $callback = self::fieldItemNormalizerCallback($fieldItemList->getFieldDefinition());
    $normalized = array_map($callback, $itemValues);
    return $normalized;
  }

  /**
   * Get field item normalizer callback.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field definition.
   *
   * @return \Closure
   *   The callback.
   */
  private static function fieldItemNormalizerCallback(FieldDefinitionInterface $fieldDefinition): \Closure {
    $propertyDefinitions = $fieldDefinition
      ->getFieldStorageDefinition()
      ->getPropertyDefinitions();
    $nonComputedProperties = array_filter($propertyDefinitions, function (DataDefinitionInterface $property) {
      return !$property->isComputed();
    });
    $callback = function ($value) use ($nonComputedProperties) {
      if (is_array($value)) {
        $value = array_intersect_key($value, $nonComputedProperties);
        ksort($value);
      }
      return $value;
    };
    return $callback;
  }

}
